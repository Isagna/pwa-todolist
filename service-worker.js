console.log('Service worker chargé');

const excludeFromCache = [

];
const cacheVersion = 'v1';

self.addEventListener('fetch', function(event) {
    const url = new URL(event.request.url);
    const link = `${url.origin}${url.pathname}`;

    if (event.request.method === 'GET' && !excludeFromCache.includes(link)) {
        event.respondWith(
            caches.match(event.request)
                .then(function(response) {clients
                    return response || fetch(event.request)
                        .then(function(response) {
                            const responseClone = response.clone();
                            caches.open(cacheVersion)
                                .then(function(cache) {
                                    cache.put(event.request, responseClone);
                                })

                            return response;
                        })
                })
                .catch(function() {
                    return cache.match('index.html');
                })
        );
    }
});
