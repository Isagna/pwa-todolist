import page from 'page';
import { fetchTodos } from './api/todos.js';
import { setTodos, getTodos } from './idb.js';

const app = document.querySelector('#app');
fetch('./config.json')
    .then(result => result.json())
    .then(async (config) => {
        window.config = config
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = './assets/styles/tailwind.css';
        document.head.appendChild(link);

        page('/', async (ctx) => {
            const module = await import('./views/Home.js');
            const Home = module.default;

            let todos = [];
            if (navigator.onLine) {
                todos = await fetchTodos();
                //let localTodos = await setTodos(todos);
                console.log('Online:', todos);
            } else {
                todos = await getTodos();
                console.log('Offline:', todos);
            }

            const ctn = app.querySelector('[page="home"]');
            const HomeView = new Home(ctn);



            HomeView.todos = todos;
            HomeView.renderView();
            // render second time to get event listener on, it's must be done as fetch database are async
            HomeView.renderView();

            ctn.setAttribute('active', true);
        });

        // Start router
        page();

    });