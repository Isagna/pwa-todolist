import { html, render } from 'lit-html';

export default class Home {
  constructor(page) {
    this.page = page;
    this.properties = {
      todos: [
      ],
      todo: '',
      eventAdded: false
    };

    this.renderView();
    this.handleForm = this.handleForm.bind(this);
    this.handleDone = this.handleDone.bind(this);

  }

  set todos(value) {
    this.properties.todos = value;
  }

  get todos() {
    return this.properties.todos;
  }

  todoCard (todo) {
    let pivot = this;
    let card = html`
      <div class="todo-card flex text-center">
        <p class="w-2/3">${todo.title}</p>
        <p class="w-1/6"><button class="done-button" id="done-button-${todo.id}" value="${todo.id}"> ${ todo.isDone ? 'fait': 'à faire'}</button></p>
        <p class="w-1/6"><button class="delete-button">supprimer</button></p>
      </div>
    `;

    // check event alreadé adde, otherwise event will be add at each renderView
    if (!this.properties.eventAdded) {
      let doneButton = document.getElementById('done-button-' + todo.id);

      if (doneButton) {

        doneButton.addEventListener('click', function () {
          pivot.handleDone(todo);
        });
      }
    }

    return card;
  }

  // Handle click on "Fait | Pas fait"
  handleDone (todo) {
    todo.isDone = !todo.isDone;

    fetch('http://localhost:3000/todos/' + todo.id , {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PUT',
      body: JSON.stringify(todo)
    }).then( (response) => {
      this.properties.eventAdded = true;
      this.renderView();

    });

  }

  template() {
    return html`
      <section class="h-full text-center">
        <div ?hidden="${!this.properties.todos.length}">
          <header>
            <h1 class="mt-2 px-4 text-xl mb-5">My awesome todos </h1>
          </header>
          <main class="todolist px-4 pb-20">
              ${this.properties.todos.map( todo => this.todoCard(todo))}
          </main>
        </div>
        <div class="mt-8" ?hidden="${this.properties.todos.length}">
          <img class="object-contain px-20" src="/img/nodata.svg" alt="No data">
          <p class="mt-4 text-center text-xl">No todos yet, try to create a new one</p>
        </div>
        <footer class="h-16 bg-gray-300 fixed bottom-0 inset-x-0">
          <form @submit="${this.handleForm}" id="addTodo" class="w-full h-full flex justify-between items-center px-4 py-3">
            <label class="flex-1" aria-label="Add todo input">
              <input
                autocomplete="off"
                .value="${this.properties.todo}"
                @input="${e => this.properties.todo = e.target.value}"
                class="py-3 px-4 rounded-sm w-full h-full"
                type="text"
                placeholder="Enter a new todo ..."
                name="title">
            </label>
            <button
              aria-label="Add"
              class="ml-4 rounded-lg text-uppercase bg-heraku h-full text-center px-3 uppercase text-white font-bold flex justify-center items-center"
              type="submit">Add<lit-icon class="ml-2" icon="send"></lit-icon></button>
          </form>  
        </footer>
      </section>
    `;
  }

  renderView() {
    const view = this.template();
    render(view, this.page);
  }

  handleForm(e) {
    e.preventDefault();
    if (this.properties.todo === '') return console.log('[todo] Value is required !!!');
    const todo = {
      id: Date.now(),
      title: this.properties.todo,
      description: "to do",
      isDone: false
    };

    fetch('http://localhost:3000/todos', {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(todo)
    }).then( (response) => {
      this.todos = [...this.todos, todo];

      this.properties.todo = null;
      let input = document.querySelector('[name="title"]');
      input = '';

      this.renderView();
    });

  }

}
